/**
 * Created by andrea on 3/6/16.
 */

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.Select;

public class test_element_estados{

    private WebDriver driver;

    @Before

    public void setup() throws Exception {
        //driver =new HtmlUnitDriver();
        //driver.manage().window().maximize();
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //driver.get("http://calidad.ypsoluciones.com/andrea_final.html");
        driver = new HtmlUnitDriver(true);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        //To hide warnings logs from execution console.
        Logger logger = Logger.getLogger("");
        logger.setLevel(Level.OFF); //Opening URL In HtmlUnitDriver.
        driver.get("http://calidad.ypsoluciones.com/andrea_final.html");
    }

    @Test
    public void getSize() throws Exception {
        //Locate element for which you wants to get height and width.

        WebElement Image = driver.findElement(By.xpath("//img[@id='andrea']"));
        //Get width of element.

        int ImageWidth = Image.getSize().getWidth();
        System.out.println("Image width Is "+ImageWidth+" pixels");
        //Get height of element.
        int ImageHeight = Image.getSize().getHeight();
        System.out.println("Image height Is "+ImageHeight+" pixels"); }

}
