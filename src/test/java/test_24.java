import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;

/**
     * Created by pabloalban on 3/6/16. &sleep 20s&
 */
public class test_24 {

    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
    driver = new HtmlUnitDriver(true);
    driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
    //To hide warnings logs from execution console.
    Logger logger = Logger.getLogger("");
    logger.setLevel(Level.OFF); //Opening URL In HtmlUnitDriver.
    driver.get("http://calidad.ypsoluciones.com/pablo_final.html");

    }

    @Test
    public void test24() throws Exception {
        driver.get("http://calidad.ypsoluciones.com/pablo_final.html");
//        assertEquals("Español", driver.findElement(By.cssSelector("#ui-id-6 > ul.eco_arrowpoint > li")).getText());
        try {
            assertEquals("Español", driver.findElement(By.cssSelector("#ui-id-6 > ul.eco_arrowpoint > li")).getText());
        }  catch (Error e) {
            verificationErrors.append(e.toString());
        }
       // assertEquals("Inglés", driver.findElement(By.xpath("//div[@id='ui-id-6']/ul/li[2]")).getText());
        try {
            assertEquals("Inglés", driver.findElement(By.xpath("//div[@id='ui-id-6']/ul/li[2]")).getText());
        }  catch (Error e) {
            verificationErrors.append(e.toString());
        }
       // assertEquals("Italiano", driver.findElement(By.xpath("//div[@id='ui-id-6']/ul/li[3]")).getText());
        try {
            assertEquals("Italiano", driver.findElement(By.xpath("//div[@id='ui-id-6']/ul/li[3]")).getText());
        }  catch (Error e) {
            verificationErrors.append(e.toString());
        }
     //   assertEquals("Portugués", driver.findElement(By.xpath("//div[@id='ui-id-6']/ul/li[4]")).getText());
        try {
            assertEquals("Portugués", driver.findElement(By.xpath("//div[@id='ui-id-6']/ul/li[4]")).getText());
        }  catch (Error e) {
        verificationErrors.append(e.toString());
    }

    }


    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();

    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
