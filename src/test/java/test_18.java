/**
 * Created by andrea on 2/6/16.
 */
import java.util.StringTokenizer;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.Select;

public class test_18 {

    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        driver = new HtmlUnitDriver();
        baseUrl = "http://calidad.ypsoluciones.com/index.html";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void test18() throws Exception {
        driver.get("http://calidad.ypsoluciones.com/andrea_final.html");
        assertTrue(isElementPresent(By.cssSelector("i.fa.fa-facebook")));
        try {
            assertTrue(isElementPresent(By.cssSelector("i.fa.fa-facebook")));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        try {
            assertEquals("", driver.findElement(By.cssSelector("i.fa.fa-facebook")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
      // driver.findElement(By.cssSelector("li.eco_facebook")).click();
        driver.get("http://localhost:8080/app/andrea_final.html");
        assertTrue(isElementPresent(By.cssSelector("li.eco_twitter")));
        try {
            assertTrue(isElementPresent(By.cssSelector("li.eco_twitter")));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        try {
            assertEquals("", driver.findElement(By.xpath("//div[3]/ul/li[2]")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
//        driver.findElement(By.xpath("//li[2]/a/i")).click();
        driver.get("http://localhost:8080/app/andrea_final.html");
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
