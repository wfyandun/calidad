/**
 * Created by andrea on 3/6/16.
 */
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;


public class test_scroll_page {




        public static void main(String[] args) throws Exception {

            // load browser
            WebDriver driver=new HtmlUnitDriver();


            // maximize browser
            driver.manage().window().maximize();


            // Open Application
            driver.get("http://calidad.ypsoluciones.com/andrea_final.html");


            // Wait for 5 second
            Thread.sleep(5000);

            // This  will scroll page 400 pixel vertical
            ((JavascriptExecutor)driver).executeScript("scroll(0,400)");


        }
}
