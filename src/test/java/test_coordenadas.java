/**
 * Created by andrea on 3/6/16.
 */

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.Select;
import java.util.logging.Logger;
import java.util.logging.Level;

public class test_coordenadas {

    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
       // driver =new HtmlUnitDriver();
        /*HtmlUnitDriver driver = new HtmlUnitDriver(BrowserVersion.FIREFOX_38);
        driver.setJavascriptEnabled(true);
        driver.get("http://calidad.ypsoluciones.com/andrea_final.html");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://calidad.ypsoluciones.com/andrea_final.html");
        Logger logger = Logger.getLogger(""); logger.setLevel(Level.OFF);

        */
        driver = new HtmlUnitDriver(true);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        //To hide warnings logs from execution console.
        Logger logger = Logger.getLogger("");
        logger.setLevel(Level.OFF); //Opening URL In HtmlUnitDriver.
        driver.get("http://calidad.ypsoluciones.com/andrea_final.html");

    }

    @Test
    public void getCoordinates() throws Exception {

        //Locate element for which you wants to retrieve x y coordinates.
        WebElement Image = driver.findElement(By.xpath("//img[@id='andrea']"));

        //Used points class to get x and y coordinates of element.
        Point point = Image.getLocation();
        int xcord = point.getX();
        int govalueX = 65;
        int govalueY = 409;
        //System.out.println("Element's Position from left side Is "+xcord +" pixels.");
        int ycord = point.getY();
        //System.out.println("Element's Position from top side Is "+ycord +" pixels.");
        if (xcord == govalueX)
        {
            System.out.println("good");

        }
        else {
            System.out.println("bad");
        }
            if (ycord == govalueY)
            {
                System.out.println("good");

            }
        else {
                System.out.println("bad");
            }
        }
    }

