/**
 * Created by andrea on 3/6/16.
 */
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.Select;
import java.util.logging.Logger;
import java.util.logging.Level;

public class test_23 {

    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    @Before
    public void setUp() throws Exception {
  /*      driver = new HtmlUnitDriver();
        baseUrl = "http://calidad.ypsoluciones.com/index.html";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    */
    driver = new HtmlUnitDriver(true);
    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    //To hide warnings logs from execution console.
    Logger logger = Logger.getLogger("");
    logger.setLevel(Level.OFF); //Opening URL In HtmlUnitDriver.
    driver.get("http://calidad.ypsoluciones.com/andrea_final.html");
}

    @Test
    public void test23() throws Exception {
        driver.get("http://calidad.ypsoluciones.com/andrea_final.html");
//        assertEquals("LANGUAGES", driver.findElement(By.id("ui-id-7")).getText());
        try {
           assertEquals("LANGUAGES", driver.findElement(By.id("ui-id-7")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        try {
            assertTrue(isElementPresent(By.id("ui-id-7")));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        try {
           assertEquals("Español", driver.findElement(By.cssSelector("#ui-id-8 > ul.eco_arrowpoint > li")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        assertTrue(isElementPresent(By.cssSelector("#ui-id-8 > ul.eco_arrowpoint")));
        //assertEquals("Inglés", driver.findElement(By.xpath("//div[@id='ui-id-8']/ul/li[2]")).getText());
        try {
            assertEquals("Inglés", driver.findElement(By.xpath("//div[@id='ui-id-8']/ul/li[2]")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        //assertEquals("Italiano", driver.findElement(By.xpath("//div[@id='ui-id-8']/ul/li[3]")).getText());
        try {
            assertEquals("Italiano", driver.findElement(By.xpath("//div[@id='ui-id-8']/ul/li[3]")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
       //assertEquals("Portugués", driver.findElement(By.xpath("//div[@id='ui-id-8']/ul/li[4]")).getText());
        try {
            assertEquals("Portugués", driver.findElement(By.xpath("//div[@id='ui-id-8']/ul/li[4]")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        try {
            assertTrue(isElementPresent(By.cssSelector("#ui-id-8 > ul.eco_arrowpoint")));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
     //   if (!"".equals(verificationErrorString)) {
       //    fail(verificationErrorString);
        //}
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
